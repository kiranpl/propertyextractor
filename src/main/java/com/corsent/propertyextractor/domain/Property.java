package com.corsent.propertyextractor.domain;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Property {
    private int id;
    private String uniqueId;
    private String type;
    private String tenure;
    private String address;
    private String size;
    private String rent;
    private String agentName;
    private String agentContact;
    private String desc;
    private String postedDate;
    private String propertyUrl;
}
