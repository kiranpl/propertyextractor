package com.corsent.propertyextractor.domain;

import lombok.Getter;

@Getter
public class Config {
    private String name;
    private String baseUrl;
    private String url;
}
