package com.corsent.propertyextractor.portal;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.corsent.propertyextractor.domain.Config;
import com.corsent.propertyextractor.domain.Property;
import com.corsent.propertyextractor.Searchable;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class PropertyLink implements Searchable {

    private String configPath = "src/main/resources/PropertyLink.json";

    public List<Property> getProperties(String searchLocation) throws IOException {
        List<Property> properties = new ArrayList<Property>();

        String configJson = FileUtils.readFileToString(new File(configPath), "UTF-8");
        ObjectMapper objectMapper = new ObjectMapper();
        Config config = objectMapper.readValue(configJson, Config.class);
        String url = config.getUrl().replace("#SEARCH_LOCATION#", searchLocation)
                .replace("#LOCATION#", searchLocation.toLowerCase().
                        replace(" ", "-").
                        replace(",", "").
                        replace("(", "").
                        replace(")", ""));

        Document document;

        try {

            int id = 1;
            Elements pageElements;
            boolean nextExists;

            ExecutorService executorService = Executors.newFixedThreadPool(5);

            do {
                nextExists=false;
                //Extract details
                document = Jsoup.connect(url).get();

                String errorMessage = document.body().getElementsByClass("messages__error-search-sub-title").text();
                if (errorMessage.contains("we currently do not have any listings")) break;

                Elements elements = document.body().getElementsByClass("card-img-top-link");
                List<Future<Property>> futures = new ArrayList<Future<Property>>();

                for (Element element : elements) {
                    Future<Property> future = executorService.submit(new PropertyFetcher(id, element, config));

                    futures.add(future);

                    id++;
                }

                for (Future<Property> future : futures) {
                    Property property = future.get();
                    if (property.getUniqueId() != null)
                        properties.add(property);
                }

                //Check if next page exists
                pageElements = document.body().getElementsByClass("c-pagination__link");
                for (Element element : pageElements) {
                    if (element.text().equalsIgnoreCase(">")) {
                        nextExists=true;
                        url = config.getBaseUrl() + element.attr("href");
                        break;
                    }
                }
            } while (nextExists);

            for (Property property : properties) {
                System.out.println(property.getId() + "|" + property.getUniqueId() + "|" + property.getAddress() + "|" + property.getRent() + "|" + property.getSize() + "|" + property.getType());
            }

            executorService.shutdown();


        } catch (Exception ex) {
           ex.printStackTrace();
        }

        return properties;
    }
}

class PropertyFetcher implements Callable<Property> {

    int identifier;
    Element element;
    Config config;

    public PropertyFetcher(int identifier, Element element, Config config) {
        this.identifier = identifier;
        this.element = element;
        this.config = config;
    }

    @Override
    public Property call() throws Exception {
            Property property = new Property();
            property.setId(identifier);

            try {
                String propertyUrl = config.getBaseUrl() + element.attr("href");
                property.setPropertyUrl(propertyUrl);
                Document propertydocument = Jsoup.connect(propertyUrl).get();

                property.setUniqueId(propertydocument.body().getElementsByClass("js-arxa-item-view").attr("data-item-id"));
                String subTitle = propertydocument.body().getElementsByClass("details-advert-subtitle").text().trim();
                List<String> propertyValues = Arrays.asList(subTitle.split("\\|"));
                if (propertyValues.size() == 3) {
                    property.setRent(propertyValues.get(0).replace("To rent:", "").trim());
                    property.setSize(propertyValues.get(1).trim());
                    property.setType(propertyValues.get(2).trim());
                }
                property.setAddress(propertydocument.body().getElementsByClass("details-advert-title").text().trim());
            } catch(Exception ex) {
                //ex.printStackTrace();
            }

            return property;
    }
}
