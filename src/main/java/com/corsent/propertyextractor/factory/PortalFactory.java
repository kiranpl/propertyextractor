package com.corsent.propertyextractor.factory;

import com.corsent.propertyextractor.Searchable;
import com.corsent.propertyextractor.portal.PropertyLink;

public class PortalFactory {

    public static Searchable createInstance(String portalName) throws Exception {

        if (portalName.equalsIgnoreCase("PropertyLink")) {
            return new PropertyLink();
        } else {
            throw new Exception("Unknown portal name");
        }

    }
}
