package com.corsent.propertyextractor;

import com.corsent.propertyextractor.domain.Property;

import java.io.IOException;
import java.util.List;

public interface Searchable {
    List<Property> getProperties(String searchLocation) throws IOException;
}
