package com.corsent.propertyextractor;

import com.corsent.propertyextractor.domain.Property;
import com.corsent.propertyextractor.portal.PropertyLink;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class PropertyLinkTest {

    @Test
    public void binghamTest() throws IOException {
        PropertyLink propertyLink = new PropertyLink();

        List<Property> properties = propertyLink.getProperties("Bingham, Nottingham");

        for (Property property : properties) {
            System.out.println(property.getId() + "|" + property.getUniqueId() + "|" + property.getAddress() + "|" + property.getRent() + "|" + property.getSize() + "|" + property.getType());
        }
    }

    @Test
    public void chelmsfordTest() throws IOException {
        PropertyLink propertyLink = new PropertyLink();

        List<Property> properties = propertyLink.getProperties("Chelmsford");

        for (Property property : properties) {
            System.out.println(property.getId() + "|" + property.getUniqueId() + "|" + property.getAddress() + "|" + property.getRent() + "|" + property.getSize() + "|" + property.getType());
        }
    }
}
